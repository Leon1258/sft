'use strict';
$(document).ready(function(){

// Открыть/закрыть меню
	$('.menu-button, .menu-toggle').on('click', function(e) {
		e.preventDefault();

		var sidebar = $('.sidebar'),
			content = $('.main-content'),
			footer = $('.footer-content-wrap'),
			windowWidth = $(window).width();

		$(window).on('resize', function() {
			return windowWidth = $(window).width();
		});

		if(windowWidth > 960) {
			if(sidebar.hasClass('close')) {
				sidebar.removeClass('close');
				content.css('padding-left', '');
				footer.css('padding-left', '');

			} else {
				sidebar.addClass('close');
				content.css('padding-left', '105px');
				footer.css('padding-left', '95px')
			}
		} else if(windowWidth <= 768) {
			sidebar.removeClass('close');
			sidebar.toggleClass('show-menu');
		}
	});

});